# Sistemas de Recuperación de Información en la Web

## Práctica 3

### run python server

```
cd path/to/project/backend/python
source sriw-practica3/bin/activate
python3 server.py
```

### run node backend

```
cd path/to/project/backend/node
npm install
npm start
```

### start frontend

```
cd path/to/project/frontend
npm start
```
