import axios from "axios";

// Fetch past executions from local storage and display them in table
let movies = localStorage.getItem("movies") ?? "[]";
movies = JSON.parse(movies);
let user = {};
asyncParse(localStorage.getItem("user")).then((res) => {
  user = res;
  displayMovies(res.ratings);
});

function displayMovies(userRatings) {
  const movieTable = document.getElementById("movie-table");

  if (movieTable) {
    let rows = "";
    movies.forEach((movie) => {
      rows += `<tr><th scope='row'>${extractReadableMovieName(
        movie.title
      )}</th><td><select class="form-select form-select-sm" id='select-rating-${
        movie.title
      }' onchange="saveRating('${
        movie.title
      }')" aria-label="Default select example">
      <option id='select-none-${movie.title}' value="" ${
        !userRatings[movie.title] && "selected"
      }>Choose a rating</option>
      <option id='select-1-${movie.title}' value="1" ${
        userRatings[movie.title] && userRatings[movie.title] === 1 && "selected"
      }>1</option>
      <option id='select-2-${movie.title}' value="2" ${
        userRatings[movie.title] && userRatings[movie.title] === 2 && "selected"
      }>2</option>
      <option id='select-3-${movie.title}' value="3" ${
        userRatings[movie.title] && userRatings[movie.title] === 3 && "selected"
      }>3</option>
      <option id='select-4-${movie.title}' value="4" ${
        userRatings[movie.title] && userRatings[movie.title] === 4 && "selected"
      }>4</option>
      <option id='select-5-${movie.title}' value="5" ${
        userRatings[movie.title] && userRatings[movie.title] === 5 && "selected"
      }>5</option>
      <option id='select-6-${movie.title}' value="6" ${
        userRatings[movie.title] && userRatings[movie.title] === 6 && "selected"
      }>6</option>
      <option id='select-7-${movie.title}' value="7" ${
        userRatings[movie.title] && userRatings[movie.title] === 7 && "selected"
      }>7</option>
      <option id='select-8-${movie.title}' value="8" ${
        userRatings[movie.title] && userRatings[movie.title] === 8 && "selected"
      }>8</option>
      <option id='select-9-${movie.title}' value="9" ${
        userRatings[movie.title] && userRatings[movie.title] === 9 && "selected"
      }>9</option>
      <option id='select-10-${movie.title}' value="10" ${
        userRatings[movie.title] &&
        userRatings[movie.title] === 10 &&
        "selected"
      }>10</option>
    </select></td></tr>`;
    });

    if (rows.length === 0) {
      rows =
        "<tr><td colspan='2'>Nothing to display. Go back to the start and try to load some movies.</td></tr>";
    }

    movieTable.innerHTML = rows;
  }
}

function saveRating(movieTitle) {
  // add "\" before each "(" or ")"
  movieTitle = addBackSlashBeforeBracket(movieTitle);
  const selectionElement = document.getElementById(
    `select-rating-${movieTitle}`
  );
  // Send request to backend
  const userId = user._id;
  axios
    .patch(`http://127.0.0.1:3002/rate/${userId}`, {
      movieId: movieTitle,
      rating: selectionElement.value,
    })
    .then((res) => {
      // write current user with ratings into local storage
      localStorage.setItem("user", JSON.stringify(res.data.user));
      user = res.data.user;
    })
    .catch((err) => {
      console.log(err);
    });
}

function extractReadableMovieName(dbpediaTitle) {
  const nameArray = dbpediaTitle.split("\\");
  let movieName = nameArray[0]; // Only take the part before the first "\" (indicating brackets)
  if (movieName[movieName.length - 1] === "_") {
    movieName = movieName.substring(0, movieName.length - 1); // Cut the trailing "_"
  }
  return movieName;
}

function addBackSlashBeforeBracket(word) {
  let result = "";
  for (let i = 0; i < word.length; i++) {
    const char = word[i];
    if (char === "(" || char === ")") {
      result += "\\";
    }
    result += char;
  }
  return result;
}

function asyncParse(str) {
  return new Promise((resolve, reject) => {
    resolve(JSON.parse(str));
  });
}

function handleGetRecommendations() {
  // Get ratings from all users
  axios
    .get("http://127.0.0.1:3002/ratings")
    .then((res) => {
      const allRatings = res.data;
      // Create user profile from ratings and give back 5 closest users
      axios
        .post(`http://127.0.0.1:5000/getUserProfile`, {
          movies: localStorage.getItem("movies"),
          ratings: JSON.stringify(user.ratings),
          allRatings: allRatings,
          uid: user._id,
        })
        .then((res2) => {
          const distances = res2.data.distances;
          console.log(distances);
          localStorage.setItem("distances", JSON.stringify(distances));
          console.log(allRatings);

          // Choose max 5 users with lowest distance
          const distanceNumbers = [];
          for (let i = 0; i < distances.length; i++) {
            const dist = JSON.stringify({
              index: i,
              dist: distances[i]["dist"],
            });
            distanceNumbers.push(dist);
          }

          console.log(distanceNumbers);

          const minIndices = [];

          while (
            minIndices.length < 5 &&
            distanceNumbers.length - minIndices.length > 0
          ) {
            let minIndex;
            let minDist = Number.MAX_SAFE_INTEGER;
            for (const distance of distanceNumbers) {
              const distanceParsed = JSON.parse(distance);
              if (!minIndices.includes(distanceParsed.index)) {
                const dist = distanceParsed["dist"];
                if (dist < minDist) {
                  minDist = dist;
                  minIndex = distanceParsed.index;
                }
              }
            }
            minIndices.push(minIndex);
            console.log(minIndices);
            console.log(distanceNumbers);
          }

          // Get the average rating for each movie from all these users
          const movies = JSON.parse(localStorage.getItem("movies"));
          const avg_ratings = [];
          for (const movie of movies) {
            const ratings = [];
            for (const index of minIndices) {
              const uid = distances[index]["toCompareUid"];
              const userRatings = allRatings["ratings"].filter(
                (r) => r["userId"] === uid
              );
              if (
                movie.title in userRatings[0]["ratings"] &&
                userRatings[0]["ratings"][movie.title] != null
              ) {
                ratings.push(userRatings[0]["ratings"][movie.title]);
              }
            }
            const sum = ratings.reduce((a, b) => a + b, 0);
            const avg = sum / ratings.length || 0;
            avg_ratings.push({ title: movie.title, avgRating: avg });
          }

          console.log(avg_ratings);

          // Filter out those movies that the user has not already seen
          const userRatings = user.ratings;
          console.log(userRatings);
          const potentialCandidates = [];
          for (const movie of avg_ratings) {
            if (
              !(movie.title in userRatings) ||
              (movie.title in userRatings && userRatings[movie.title] == null)
            ) {
              potentialCandidates.push(movie);
            }
          }

          console.log(potentialCandidates);

          // Display the top 3 movies
          const avgRatingNumbers = [];
          for (let i = 0; i < potentialCandidates.length; i++) {
            const rating = JSON.stringify({
              index: i,
              avgRating: potentialCandidates[i]["avgRating"],
            });
            avgRatingNumbers.push(rating);
          }

          console.log(avgRatingNumbers);

          const maxIndices = [];

          while (
            maxIndices.length < 3 &&
            avgRatingNumbers.length - maxIndices.length > 0
          ) {
            let maxIndex;
            let maxRating = Number.MIN_SAFE_INTEGER;
            for (const rating of avgRatingNumbers) {
              const ratingParsed = JSON.parse(rating);
              if (!maxIndices.includes(ratingParsed.index)) {
                const avgRating = ratingParsed["avgRating"];
                if (avgRating > maxRating) {
                  maxRating = avgRating;
                  maxIndex = ratingParsed.index;
                }
              }
            }
            maxIndices.push(maxIndex);
            console.log(maxIndices);
          }

          let tContent = "";

          for (const maxIndex of maxIndices) {
            tContent += `<tr><td>${extractReadableMovieName(
              potentialCandidates[maxIndex].title
            )}</td><td>${potentialCandidates[maxIndex].avgRating}</td></tr>`;
          }

          const recommendations = document.getElementById("recommendations");
          recommendations.innerHTML = `<h4 class="mt-5 mb-2">These are your personal recommendations:</h4><table class="table"><thead><tr> <th scope="col">Title</th>
          <th scope="col">Avg. rating</th></tr></thead><tbody>${tContent}</tbody></table>`;
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
}

window.handleGetRecommendations = handleGetRecommendations;
window.saveRating = saveRating;
