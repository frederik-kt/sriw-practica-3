import axios from "axios";
import "./profile";

const loginForm = document.getElementById("loginForm");
if (loginForm) {
  loginForm.addEventListener("submit", handleLogin, false);
}

const registerForm = document.getElementById("registerForm");
if (registerForm) {
  registerForm.addEventListener("submit", handleRegister, false);
}

const handleGetItems = () => {
  const getItemsButton = document.getElementById("getItemsButton");
  getItemsButton.disabled = true;
  getItemsButton.innerHTML =
    "<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span><span class='sr-only'>  Get items from DBpedia</span>";
  // Calling python server to get dbpedia items
  axios
    .get(`http://127.0.0.1:5000/getDBPediaItems`)
    .then((res) => {
      getItemsButton.disabled = false;
      getItemsButton.innerText = "Get Items from DBpedia";
      const getItemsSuccess = document.getElementById("getItemsSuccess");
      getItemsSuccess.innerText = "Success!";
      localStorage.setItem("movies", JSON.stringify(res.data.result));
    })
    .catch((err) => {
      const getItemsError = document.getElementById("getItemsError");
      getItemsError.innerText = err.message;
      console.log(err);
    });
};

function handleLogin(e) {
  e.preventDefault();
  const email = document.getElementById("loginEmail").value;
  axios
    .post(`http://127.0.0.1:3002/login`, { email: email })
    .then((res) => {
      // write current user with ratings into local storage
      localStorage.setItem("user", JSON.stringify(res.data.user));
      // route to profile
      window.location.href = "./profile.html";
    })
    .catch((err) => {
      const loginError = document.getElementById("loginError");
      loginError.innerText = err.response.data.message;
    });
}

function handleRegister(e) {
  e.preventDefault();
  const email = document.getElementById("registerEmail").value;
  axios
    .post(`http://127.0.0.1:3002/register`, { email: email })
    .then((res) => {
      const registerSuccess = document.getElementById("registerSuccess");
      registerSuccess.innerText = "Success!";
    })
    .catch((err) => {
      const registerError = document.getElementById("registerError");
      registerError.innerText = err.response.data.message;
    });
}

window.handleGetItems = handleGetItems;
window.handleLogin = handleLogin;
