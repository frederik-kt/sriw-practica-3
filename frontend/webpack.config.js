const path = require("path");

module.exports = {
  mode: "development",
  entry: path.resolve(__dirname, "js/index.js"),
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "bundle.js",
    publicPath: "/",
  },
  devtool: "inline-source-map",
  devServer: {
    port: 5001,
    historyApiFallback: true,
    open: true,
  },
};
