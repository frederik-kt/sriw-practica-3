import json

def createUserProfile(movies, ratings, allRatings, uid):
    movies = json.loads(movies)
    ratings = json.loads(ratings)
    allRatings = allRatings["ratings"]

    # Go through ratings of original user and kick out none values
    ratingsNew = ratings.copy()
    for k in ratings:
        if ratings[k] is None:
            del ratingsNew[k]

    # Go through allRatings and for every user get an array of those movies that the original user has also rated
    toCompare = []
    for rating in allRatings:
        if rating["userId"] != uid:
            ratingInfo = rating["ratings"]
            ratingInfoNew = ratingInfo.copy()
            for k in ratingInfo:
                if not k in ratingsNew:
                    del ratingInfoNew[k]
                elif ratingInfo[k] is None:
                    del ratingInfoNew[k]
            toCompare.append({"userId": rating["userId"], "ratings": ratingInfoNew})

    # For each other user calculate a profile for the original user based only on the movies that are present in both
    comparisons = []
    for rating in toCompare:
        originalRatings = ratingsNew.copy()
        for k in ratingsNew:
            if not k in rating["ratings"]:
                del originalRatings[k]
        print(originalRatings)

        # Setup a vector containing the ratings for all movies
        ratingsVectorOriginal = []
        ratingsVectorToCompare = []
        ratingsSumOriginal = 0
        ratingsSumToCompare = 0
        for movie in movies:
            movieTitle = movie["title"]
            userRatingOriginal = None
            userRatingToCompare = None
            if movieTitle in originalRatings:
                userRatingOriginal = originalRatings[movieTitle]
                userRatingToCompare = rating["ratings"][movieTitle]
                ratingsSumOriginal += userRatingOriginal
                ratingsSumToCompare += userRatingToCompare
            ratingsVectorOriginal.append(userRatingOriginal)
            ratingsVectorToCompare.append(userRatingToCompare)
    
        # Setup a matrix of movies with fields budget, runtime
        MovieMatrix1 = [["budget", "runtime"]]
        maxBudget = 0
        minBudget = 9999999999999999
        maxRuntime = 0
        minRuntime = 9999999999999999
        for movie in movies:
            budget = movie["budget"]
            print(movie)
            print(budget)
            # calculate number from scientific notation
            if any(c.isalpha() for c in budget):
                budget_split = budget.split("E", 1)
                firstPart = budget_split[0]
                if len(budget_split) > 1:
                    secondPart = budget_split[1]
                    budget = float(firstPart) * (10**float(secondPart))
                else:
                    budget = float(firstPart)
                    print(movie)
            else:
                budget = float(budget)
            maxBudget = max(maxBudget, budget)
            minBudget = min(minBudget, budget)
            runtime = float(movie["runtime"])
            maxRuntime = max(maxRuntime, runtime)
            minRuntime = min(minRuntime, runtime)
            MovieMatrix1.append([budget, runtime])
    
        # Setup a matrix of movies with fields [director_name], [director_name] ...
        directors = []
        for movie in movies:
            if not movie["director"] in directors:
                directors.append(movie["director"])

        MovieMatrix2 = [directors]
        for movie in movies:
            newRow = []
            for director in directors:
                if movie["director"] == director:
                    newRow.append(1)
                else:
                    newRow.append(0)
            
            MovieMatrix2.append(newRow)

        # Setup a matrix of movies with fields [actor_name], [actor_name] ...
        actors = []
        for movie in movies:
            for actor in movie["starring"]:
                if not actor in actors:
                    actors.append(actor)

        MovieMatrix3 = [actors]
        for movie in movies:
            newRow = []
            for actor in actors:
                if actor in movie["starring"]:
                    newRow.append(1)
                else:
                    newRow.append(0)
            
            MovieMatrix3.append(newRow)
    
        # Multiply the ratings for a movie with each field for that movie
        MovieMatrix1_mult_original = MovieMatrix1.copy()
        MovieMatrix1_mult_toCompare = MovieMatrix1.copy()
        MovieMatrix2_mult_original = MovieMatrix2.copy()
        MovieMatrix2_mult_toCompare = MovieMatrix2.copy()
        MovieMatrix3_mult_original = MovieMatrix3.copy()
        MovieMatrix3_mult_toCompare = MovieMatrix3.copy()
        for i, ratingInfo in enumerate(ratingsVectorOriginal):
            ratingUsed = ratingInfo
            if ratingInfo is None:
                ratingUsed = 0
            newRow = list(MovieMatrix1[i + 1])
            for idx, entry in enumerate(newRow):
                newRow[idx] = entry * ratingUsed
            MovieMatrix1_mult_original[i + 1] = newRow

            newRow = list(MovieMatrix2[i + 1])
            for idx, entry in enumerate(newRow):
                newRow[idx] = entry * ratingUsed
            MovieMatrix2_mult_original[i + 1] = newRow

            newRow = list(MovieMatrix3[i + 1])
            for idx, entry in enumerate(newRow):
                newRow[idx] = entry * ratingUsed
            MovieMatrix3_mult_original[i + 1] = newRow

        for i, ratingInfo in enumerate(ratingsVectorToCompare):
            ratingUsed = ratingInfo
            if ratingInfo is None:
                ratingUsed = 0
            newRow = list(MovieMatrix1[i + 1])
            for idx, entry in enumerate(newRow):
                newRow[idx] = entry * ratingUsed
            MovieMatrix1_mult_toCompare[i + 1] = newRow

            newRow = list(MovieMatrix2[i + 1])
            for idx, entry in enumerate(newRow):
                newRow[idx] = entry * ratingUsed
            MovieMatrix2_mult_toCompare[i + 1] = newRow

            newRow = list(MovieMatrix3[i + 1])
            for idx, entry in enumerate(newRow):
                newRow[idx] = entry * ratingUsed
            MovieMatrix3_mult_toCompare[i + 1] = newRow

        # Create vector that creates the summed up value for each field
        vector1_original = []
        for row in MovieMatrix1_mult_original[1:]:
            for i, field in enumerate(row):
                if len(vector1_original) < (i + 1):
                    vector1_original.append(field)
                else:
                    vector1_original[i] += field

        vector1_toCompare = []
        for row in MovieMatrix1_mult_toCompare[1:]:
            for i, field in enumerate(row):
                if len(vector1_toCompare) < (i + 1):
                    vector1_toCompare.append(field)
                else:
                    vector1_toCompare[i] += field

        vector2_original = []
        for row in MovieMatrix2_mult_original[1:]:
            for i, field in enumerate(row):
                if len(vector2_original) < (i + 1):
                    vector2_original.append(field)
                else:
                    vector2_original[i] += field

        vector2_toCompare = []
        for row in MovieMatrix2_mult_toCompare[1:]:
            for i, field in enumerate(row):
                if len(vector2_toCompare) < (i + 1):
                    vector2_toCompare.append(field)
                else:
                    vector2_toCompare[i] += field

        vector3_original = []
        for row in MovieMatrix3_mult_original[1:]:
            for i, field in enumerate(row):
                if len(vector3_original) < (i + 1):
                    vector3_original.append(field)
                else:
                    vector3_original[i] += field

        vector3_toCompare = []
        for row in MovieMatrix3_mult_toCompare[1:]:
            for i, field in enumerate(row):
                if len(vector3_toCompare) < (i + 1):
                    vector3_toCompare.append(field)
                else:
                    vector3_toCompare[i] += field

        # For the first vector (budget & runtime) normalize both by dividing through the sum of ratings and then using min and max values to squeeze into the interval from 0 to 1.
        for i, entry in enumerate(vector1_original):
            newEntry = entry / ratingsSumOriginal
            if i == 0:
                newEntry = (newEntry - minBudget) / (maxBudget - minBudget)
            if i == 1:
                newEntry = (newEntry - minRuntime) / (maxRuntime - minRuntime)
            vector1_original[i] = newEntry
        
        for i, entry in enumerate(vector1_toCompare):
            newEntry = entry / ratingsSumToCompare
            if i == 0:
                newEntry = (newEntry - minBudget) / (maxBudget - minBudget)
            if i == 1:
                newEntry = (newEntry - minRuntime) / (maxRuntime - minRuntime)
            vector1_toCompare[i] = newEntry

        # For the second vector (directors) normalize by dividing each element by the sum of all elements and then using min and max values to squeeze into the interval from 0 to 1.
        entrySumOriginal = 0
        for entry in vector2_original:
            entrySumOriginal += entry

        for i, entry in enumerate(vector2_original):
            newEntry = entry / entrySumOriginal
            vector2_original[i] = newEntry

        entrySumToCompare = 0
        for entry in vector2_toCompare:
            entrySumToCompare += entry

        for i, entry in enumerate(vector2_toCompare):
            newEntry = entry / entrySumToCompare
            vector2_toCompare[i] = newEntry

        # For the third vector (actors) normalize by dividing each element by the sum of all elements and then using min and max values to squeeze into the interval from 0 to 1.
        entrySumOriginal = 0
        for entry in vector3_original:
            entrySumOriginal += entry

        for i, entry in enumerate(vector3_original):
            newEntry = entry / entrySumOriginal
            vector3_original[i] = newEntry

        entrySumToCompare = 0
        for entry in vector3_toCompare:
            entrySumToCompare += entry

        for i, entry in enumerate(vector3_toCompare):
            newEntry = entry / entrySumToCompare
            vector3_toCompare[i] = newEntry

        profile_original = vector1_original + vector2_original + vector3_original
        profile_original_rounded = [round(num, 3) for num in profile_original]
        profile_toCompare = vector1_toCompare + vector2_toCompare + vector3_toCompare
        profile_toCompare_rounded = [round(num, 3) for num in profile_toCompare]
        columns = MovieMatrix1[0] + MovieMatrix2[0] + MovieMatrix3[0]

        newComparison = {"originalProfile": {"uid": uid, "columns": columns, "profile": profile_original_rounded}, "toCompare": {"uid": rating["userId"], "columns": columns, "profile": profile_toCompare_rounded}}
        comparisons.append(newComparison)

    return comparisons