from SPARQLWrapper import SPARQLWrapper, JSON, N3

dbpediaSparql = SPARQLWrapper("https://dbpedia.org/sparql")
movies = ["Dune_\(2021_film\)", "Arrival_\(film\)", "Ratatouille_\(film\)", "Heat_\(1986_film\)", "Midsommar", "The_Florida_Project", "Fear_and_Loathing_in_Las_Vegas_\(film\)", "The_Thing_\(1982_film\)", "Portrait_of_a_Lady_on_Fire", "1917_\(2019_film\)", "Her_\(film\)", "Donnie_Darko", "Trainspotting_\(film\)", "Jurassic_World", "Hot_Tub_Time_Machine", "Whiplash_\(2014_film\)", "Drive_\(2011_film\)", "Casino_Royale_\(2006_film\)", "XXX:_Return_of_Xander_Cage"]

def getDBPediaItems():
    results = []
    for movie in movies:
        # Get runtime, director, budget
        queryString1 = "SELECT ?runtime ?director ?budget WHERE {dbr:" + movie + " dbo:Work\/runtime ?runtime. dbr:" + movie + " dbo:director ?director. dbr:" + movie + " dbp:budget ?budget. } LIMIT 1"
        dbpediaSparql.setQuery(queryString1)
        dbpediaSparql.setReturnFormat(JSON)
        dbpediaRes1 = dbpediaSparql.query().convert()
        # print(dbpediaRes1["results"]["bindings"][0])
        # Get 3 lead actors
        queryString2 = "SELECT ?starring WHERE {dbr:" + movie + " dbo:starring ?starring. } LIMIT 3"
        dbpediaSparql.setQuery(queryString2)
        dbpediaSparql.setReturnFormat(JSON)
        dbpediaRes2 = dbpediaSparql.query().convert()
        # print(dbpediaRes2["results"]["bindings"][1]["starring"]["value"])
        actors = []
        for res in dbpediaRes2["results"]["bindings"]:
            actors.append(res["starring"]["value"].split("http://dbpedia.org/resource/")[1])
        if not len(dbpediaRes1["results"]["bindings"]) > 0:
            print(queryString1)
        result = {
            "title": movie,
            "runtime": dbpediaRes1["results"]["bindings"][0]["runtime"]["value"],
            "director": dbpediaRes1["results"]["bindings"][0]["director"]["value"].split("http://dbpedia.org/resource/")[1],
            "budget": dbpediaRes1["results"]["bindings"][0]["budget"]["value"],
            "starring": actors
        }
        results.append(result)

    return {"result": results}
 