from flask import Flask, request
from flask_cors import CORS
import getDBPedia
import createUserProfile
import createRecommendations
app = Flask(__name__)
CORS(app)

@app.route("/getDBPediaItems", methods=['GET'])
def getDBPediaItems():
    response = getDBPedia.getDBPediaItems()
    return response

@app.route("/getUserProfile", methods=['POST'])
def getUserProfile():
    json_data = request.json
    movies = json_data["movies"]
    ratings = json_data["ratings"]
    allRatings = json_data["allRatings"]
    uid = json_data["uid"]
    comparisons = createUserProfile.createUserProfile(movies, ratings, allRatings, uid)
    response = createRecommendations.createRecommendations(comparisons)
    return response

if __name__ == "__main__":
    app.run()