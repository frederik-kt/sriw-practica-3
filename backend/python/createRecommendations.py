import json

def distance(originalProfile, toCompareProfile):
    """Distance between two vectors."""
    squares = [(p-q) ** 2 for p, q in zip(originalProfile, toCompareProfile)]
    return sum(squares) ** .5

def createRecommendations(comparisons):
    distances = []
    for comparison in comparisons:
        originalProfile = comparison["originalProfile"]["profile"]
        toCompareProfile = comparison["toCompare"]["profile"]

        dist = distance(originalProfile, toCompareProfile)  
        newDist = {"originalUid": comparison["originalProfile"]["uid"], "toCompareUid": comparison["toCompare"]["uid"], "dist": dist}
        distances.append(newDist)
    
    return {"distances": distances}