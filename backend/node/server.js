import dotenv from "dotenv";
dotenv.config();
import { connection } from "./src/db.js";
import express from "express";
const app = express();
import morgan from "morgan";
import { router } from "./src/api/routes/routes.js";
import apiErrorHandler from "./src/api/error/apiErrorHandler.js";

connection();

app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false })); // Body-parser now included in express; we want to parse urlencoded bodies; extended: false only allows for simple bodies
app.use(express.json()); // Another body-parser middleware that extracts json data and makes it easily readable

// Add header to response to allow cross-origin access, res is not sent yet but already attached so the new headers are there when res is finally sent
// "*" -> allow access to everyone
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Auth-Token"
  );

  // Browser will always sent OPTIONS request before POST or PUT to check whether it is allowed to do that.
  if (req.method === "OPTIONS") {
    // In that case additionally reply with the allowed methods
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({}); // No need to even go to routes, just return immediately
  }
  next();
});

// Routes handling requests
app.use("/", router);

app.use(apiErrorHandler);

const port = 3002;
app.listen(port);
console.log("App listening on port " + port);
