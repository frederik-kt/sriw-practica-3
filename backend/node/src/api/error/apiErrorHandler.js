import ApiError from "./ApiError.js";

const apiErrorHandler = (err, req, res, next) => {
  // In production, don't use console.log or console.error because it is not async
  console.error(err);

  if (err instanceof ApiError) {
    res.status(err.code).json({ message: err.message });
    return;
  }

  // Not sure what to do with that error, just return standard 500
  res.status(500).json({ message: "Something went wrong." });
};

export default apiErrorHandler;
