import express from "express";
const router = express.Router();
import mongoose from "mongoose";
import ApiError from "../error/ApiError.js";
import { Profile } from "../models/profile.js";
import { User } from "../models/user.js";

// Register a new user
router.post("/register", (req, res, next) => {
  const { email } = req.body;

  // Simple validation
  if (!email) {
    next(ApiError.badRequest("Please fill out all fields."));
    return;
  }

  // Check for existing user
  User.findOne({ email }).then((user) => {
    if (user) {
      next(ApiError.badRequest("User already exists."));
      return;
    }
    // Construct new User from the mongoose model
    const newUser = new User({
      _id: new mongoose.Types.ObjectId(),
      email,
      ratings: new Map(),
    });

    // Method provided by mongoose, saves in the database
    newUser
      .save()
      .then((user) => {
        res.status(201).json({
          user: {
            _id: user._id,
            email: user.email,
            ratings: user.ratings,
          },
        });
      })
      .catch((err) => {
        next(ApiError.internal(err.message));
      });

    // Also add an entry to the profiles collection
    const newProfile = new Profile({
      _id: new mongoose.Types.ObjectId(),
      uid: newUser._id,
      profile: " ",
    });

    newProfile
      .save()
      .then((profile) => {
        res.status(201).json({
          profile: {
            uid: profile.uid,
            profile: profile.profile,
          },
        });
      })
      .catch((err) => {
        next(ApiError.internal(err.message));
      });
  });
});

// Login
router.post("/login", (req, res, next) => {
  const { email } = req.body;

  // Simple validation
  if (!email) {
    next(ApiError.badRequest("Please fill out all fields."));
    return;
  }

  // Check for existing user
  User.findOne({ email })
    .then((user) => {
      if (!user) {
        next(ApiError.notFound("User does not exist."));
        return;
      }
      res.status(200).json({
        user: {
          _id: user._id,
          email: user.email,
          ratings: user.ratings,
        },
      });
    })
    .catch((err) => {
      next(ApiError.internal(err.message));
    });
});

// Get all ratings
router.get("/ratings", (req, res, next) => {
  // Find all users
  User.find()
    .exec()
    .then((docs) => {
      const response = {
        ratings: docs.map((doc) => {
          return {
            userId: doc._id,
            ratings: doc.ratings,
          };
        }),
      };
      res.status(200).json(response);
    })
    .catch((err) => {
      next(ApiError.internal(err.message));
    });
});

// Add a new rating
router.patch("/rate/:userId", (req, res, next) => {
  const userId = req.params.userId;
  const newRating = req.body;

  User.findById(userId)
    .then((user) => {
      const ratings = user.ratings;
      console.log(ratings);
      ratings.set(`${newRating.movieId}`, newRating.rating);
      console.log(ratings);
      user.ratings = ratings;

      // Method provided by mongoose, saves in the database
      user
        .save()
        .then((user) => {
          res.status(201).json({
            user: {
              _id: user._id,
              email: user.email,
              ratings: user.ratings,
            },
          });
        })
        .catch((err) => {
          next(ApiError.internal(err.message));
        });
    })
    .catch((err) => {
      next(ApiError.internal(err.message));
    });
});

// Add a new profile
router.patch("/profile/:uid", (req, res, next) => {
  const uid = req.params.uid;
  const newProfile = req.body.profile;

  Profile.findOne({ uid })
    .then((profile) => {
      profile.profile = newProfile;

      // Method provided by mongoose, saves in the database
      profile
        .save()
        .then((profile) => {
          res.status(201).json({
            profile: {
              uid: profile.uid,
              profile: profile.profile,
            },
          });
        })
        .catch((err) => {
          next(ApiError.internal(err.message));
        });
    })
    .catch((err) => {
      next(ApiError.internal(err.message));
    });
});

// Get all profiles
router.get("/profiles", (req, res, next) => {
  // Find all profiles
  Profile.find()
    .exec()
    .then((docs) => {
      const response = {
        profiles: docs.map((doc) => {
          return {
            uid: doc.uid,
            profile: doc.profile,
          };
        }),
      };
      res.status(200).json(response);
    })
    .catch((err) => {
      next(ApiError.internal(err.message));
    });
});

export { router };
