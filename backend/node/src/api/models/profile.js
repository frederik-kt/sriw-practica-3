import mongoose from "mongoose";

const profileSchema = mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    uid: { type: String, required: true, unique: true },
    profile: {
      type: String,
      required: true,
    },
  },
  { collection: "profiles" }
);

const Profile = mongoose.model("Profile", profileSchema);

export { Profile };
