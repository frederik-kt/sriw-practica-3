import mongoose from "mongoose";

const userSchema = mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    email: { type: String, required: true, unique: true },
    ratings: {
      type: Map,
      of: Number,
    },
  },
  { collection: "users" }
);

const User = mongoose.model("User", userSchema);

export { User };
