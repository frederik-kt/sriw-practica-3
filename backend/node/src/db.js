import mongoose from "mongoose";

async function connection() {
  try {
    const connectionParams = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };
    console.log("trying to connect to ", process.env.DB);
    await mongoose.connect(process.env.DB, connectionParams);
    console.log("connected to database");
  } catch (error) {
    console.log(error);
    console.log("could not connect to database");
  }
}

export { connection };
